#
# Copyright 2020 The PixelROM Project
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# BOARD_PLAT_PRIVATE_SEPOLICY_DIR += \
    vendor/google/sepolicy/private

# Include soong namespace with apex packages
PRODUCT_SOONG_NAMESPACES += \
	vendor/google/packages/apex

# Prebuilt module SDKs require prebuilt modules to work, and currently
# prebuilt modules are only provided for com.google.android.xxx.
MODULE_BUILD_FROM_SOURCE := false

# Path to Android Verified Boot Recovery key that is required
# as per Android R non-A/B requirements.
BOARD_AVB_RECOVERY_KEY_PATH := vendor/google/security/avb/recovery_avb_rsa2048.pem

# Path to OTA verification key. All our builds will be signed with this key.
# PRODUCT_EXTRA_RECOVERY_KEYS := vendor/google/security/otakey

# Path to special prop files
TARGET_PRODUCT_PROP += vendor/google/board/product.prop
TARGET_VENDOR_PROP += vendor/google/board/vendor.prop

# Overlays
DEVICE_PACKAGE_OVERLAYS += vendor/google/overlay
PRODUCT_PACKAGE_OVERLAYS += vendor/google/overlay_product
PRODUCT_PACKAGES += \
    FilesOverlay \
    FontArbutusSource \
    FontArvoLato \
    FontRubikRubik \
    FontKai \
    FontVictor \
    FontSam \
    GoogleConfigOverlay \
    GooglePermissionControllerOverlay \
    GoogleWebViewOverlay \
    PixelConfigOverlay2018 \
    PixelConfigOverlay2019 \
    PixelConfigOverlay2019Midyear \
    PixelConfigOverlayCommon \
    PixelDocumentsUIGoogleOverlay \
    PixelSetupWizardOverlay \
    PixelSetupWizardOverlay2019 \
    PixelSetupWizard__auto_generated_rro_product \
    SettingsGoogleOverlayPixel \
    SettingsGoogleOverlayRedfin \
    SettingsGoogle__auto_generated_rro_vendor \
    SystemUIGoogle__auto_generated_rro_product \
    SystemUIGoogle__auto_generated_rro_vendor \
    SystemUIGXOverlay

# Networkstack certificate
PRODUCT_MAINLINE_SEPOLICY_DEV_CERTIFICATES := vendor/google/networkstack

#PRODUCT_PACKAGES += \
    com.google.android.appsearch \
    com.google.android.wifi

# Google apex packages
PRODUCT_PACKAGES += \
    com.google.android.adbd \
    com.google.android.appsearch \
    com.google.android.art \
    com.google.android.cellbroadcast \
    com.google.android.conscrypt \
    com.google.android.extservices \
    com.google.android.ipsec \
    com.google.android.media \
    com.google.android.mediaprovider \
    com.google.android.media.swcodec \
    com.google.android.neuralnetworks \
    com.google.android.os.statsd \
    com.google.android.permission \
    com.google.android.resolv \
    com.google.android.scheduling \
    com.google.android.sdkext \
    com.google.android.telephony \
    com.google.android.tethering \
    com.google.android.tzdata3 \
    com.google.android.wifi \
    com.google.mainline.primary.libs

# QuickAccessWallet
PRODUCT_PACKAGES += \
    QuickAccessWallet

# ModuleMetadata Google
PRODUCT_PACKAGES += \
    ModuleMetadataGoogle

# Inherit from Flipendo config
$(call inherit-product, vendor/google/build/target/product/flipendo.mk)

# Include Google fonts
$(call inherit-product, vendor/google/data/fonts/fonts.mk)

# Include Google audio
$(call inherit-product, vendor/google/data/sounds/GoogleAudio.mk)

# Include default wallpaper
$(call inherit-product, vendor/google/data/wallpaper/wallpaper.mk)

# Include Google bootanimation
$(call inherit-product, vendor/google/bootanimation/bootanimation.mk)

# Include GMS
$(call inherit-product-if-exists, vendor/google/apps/pixel_gms.mk)
